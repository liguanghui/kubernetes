#!/bin/bash
BRANCH=$(git rev-parse --abbrev-ref HEAD)
nerdctl -n k8s.io image build --build-arg BRANCH=$BRANCH --no-cache -t registry.cn-zhangjiakou.aliyuncs.com/imissyou/kube-scheduler:$BRANCH .
nerdctl -n k8s.io image push registry.cn-zhangjiakou.aliyuncs.com/imissyou/kube-scheduler:$BRANCH